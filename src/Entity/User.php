<?php

class User
{
    private $name;

    private $comments;

    public function getName() : string
    {
        return $this->name;
    }

    public function setName(string $name)
    {
        $this->name = $name;
    }

    /**
     * @return Comment[]
     */
    public function getComments() : array
    {
        return $this->comments;
    }

    public function addComment(Comment $comment)
    {
        $this->comments[] = $comment;
        $comment->setAuthor($this);
    }
}
