<?php

class InMemoryCommentsRepository
{
    private $comments;

    public function store(Comment $comment)
    {
        $this->comments[] = $comment;
    }

    public function all() : array
    {
        return $this->comments;
    }
}